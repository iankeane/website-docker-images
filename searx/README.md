# Setup

Use "Make build" to build the docker image and optionally "Make run" to run the container.

If you run it in a container, you may need to remove that container from your docker daemon or it will interfere with docker-compose

# Preferences

Preferences are stored in files in the searx folder, but can be set through the web frontend as well. See the [searx documentation] (https://asciimoo.github.io/searx/admin/settings.html) for more.

# Port
    8080
