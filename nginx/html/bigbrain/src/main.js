var UFO = null;
var big = null;
var brains = [];
var time = 0;
var brainMinSpeed = 1;
var brainMaxSpeed = 8;

function init() {
  initializeUFO();
  initializeBrains();
  initializeImageWords();
}

function initializeUFO() {
  UFO = document.getElementById('myImage');
  UFO.style.position= 'relative'; 
  UFO.style.left = '0px'; 
}

function initializeImageWords() {
  big = document.getElementById('big');
  big.width = 300;
}

function initializeBrains() {
  brains = [
    {pageElement: document.getElementById('brain1'),
      speed: randomInRange(brainMinSpeed, brainMaxSpeed),
      xpos: randomInRange(0, window.outerHeight)},
    {pageElement: document.getElementById('brain2'),
      speed: randomInRange(brainMinSpeed, brainMaxSpeed),
      xpos: randomInRange(0, window.innerHeight)},
    {pageElement: document.getElementById('brain3'),
      speed: randomInRange(brainMinSpeed, brainMaxSpeed),
      xpos: randomInRange(0, window.innerHeight)},
    {pageElement: document.getElementById('brain4'),
      speed: randomInRange(brainMinSpeed, brainMaxSpeed),
      xpos: randomInRange(0, window.innerHeight)},
    {pageElement: document.getElementById('brain5'),
      speed: randomInRange(brainMinSpeed, brainMaxSpeed),
      xpos: randomInRange(0, window.innerHeight)}
    // document.getElementById('brain2'),
    // document.getElementById('brain3'),
    // document.getElementById('brain4'),
    // document.getElementById('brain5')
  ];
  var i;
  for (i = 0; i < brains.length; i++){
    brains[i].pageElement.style.position = 'absolute';
    brains[i].pageElement.style.right = '160px';
    brains[i].pageElement.style.top = Math.floor(Math.random() * Math.floor(1500)) + 'px';
};

}
function mainLoop(){
  time = time + 1
  moveRight(UFO);
  growShrink(big, time);
  brainFloat(brains, time);
  setTimeout(mainLoop,20);    // call moveRight in 20msec
}
function randomInRange(min, max) {
    return Math.random() * (max - min) + min;
}

function moveRight(obj) {
  if (parseInt(obj.style.left) > window.innerWidth + 200) {
    obj.remove();
  }
  obj.style.left = parseInt(UFO.style.left) + 5 + 'px';
}

function moveLeft(obj, speed=5) {
  obj.style.right = parseInt(obj.style.right) + speed + 'px';
}

function growShrink(obj, t, speed=1/6, size=300, sizeRange=10) {
  obj.width = size + Math.sin(t * speed) * sizeRange;
}

function hoverVert (obj, t, xpos, speed=1/100, wavelength=4) {
  obj.style.top = xpos + (Math.sin(t * speed) * wavelength);
}

function brainFloat(arr, t){
  var i;
  for (i = 0; i < arr.length; i++){
    moveLeft(arr[i].pageElement, arr[i].speed); 
    hoverVert(arr[i].pageElement, t, arr[i].xpos, speed=1/12, wavelength=25);
    if (parseInt(arr[i].pageElement.style.right) > window.innerWidth + 200) {
      arr[i].speed = randomInRange(brainMinSpeed, brainMaxSpeed);
      arr[i].xpos = randomInRange(0, document.body.scrollHeight);
      arr[i].pageElement.style.right = -200;
    }
  }

}

window.onload = function(){
  init();
  mainLoop();
}

