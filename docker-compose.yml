version: "3.3"

services:

  traefik:
    image: "traefik:v2.2.2"
    container_name: "traefik"
    # environment:
    #   - DO_AUTH_TOKEN=$DO_AUTH_TOKEN
    command:
      # - "--log.level=DEBUG"
      - "--providers.docker=true"
      - "--providers.docker.exposedbydefault=false"
      - "--entrypoints.web.address=:80"
      - "--entrypoints.websecure.address=:443"
      - "--api"
      - "--metrics"
      - "--metrics.prometheus.buckets=0.1,0.3,1.2,5.0"
      # - "--certificatesresolvers.myresolver.acme.caserver=https://acme-staging-v02.api.letsencrypt.org/directory"
      - "--certificatesresolvers.myresolver.acme.caserver=https://acme-v02.api.letsencrypt.org/directory"
      - "--certificatesresolvers.myresolver.acme.httpchallenge=true"
      - "--certificatesresolvers.myresolver.acme.httpchallenge.entrypoint=web"
      # - "--certificatesresolvers.myresolver.acme.dnschallenge=true"
      - "--certificatesresolvers.myresolver.acme.email=keanerec@gmail.com"
      - "--certificatesresolvers.myresolver.acme.storage=/letsencrypt/acme.json"
      # - "--certificatesresolvers.myresolver.acme.dnschallenge.provider=digitalocean"
      # - "--certificatesResolvers.myresolver.acme.dnsChallenge.resolvers=ns1.digitalocean.com"
      # - "--certificatesresolvers.myresolver.acme.dnschallenge.delayBeforeCheck=0"
    ports:
      - "80:80"
      - "443:443"
      - "8080:8080"
    volumes:
      - "./letsencrypt:/letsencrypt"
      - "/var/run/docker.sock:/var/run/docker.sock:ro"
      - "./traefik:/etc/traefik"
    labels:
      - "traefik.enable=true"

      # middleware redirect
      - "traefik.http.middlewares.redirect-to-https.redirectscheme.scheme=https"

      # global redirect to https
      - "traefik.http.routers.redirs.rule=hostregexp(`{host:.+}`)"
      - "traefik.http.routers.redirs.entrypoints=web"
      - "traefik.http.routers.redirs.middlewares=redirect-to-https"

      # domain setup
      - "traefik.http.routers.traefik.tls=true"
      # - "traefik.http.routers.traefik.tls.domains[0].main=$DOMAINNAME"
      # - "traefik.http.routers.traefik.tls.domains[0].sans=*.$DOMAINNAME"
      # - "traefik.http.routers.traefik-rtr.tls.domains[1].main=$SECONDDOMAINNAME" # Pulls main cert for second domain
      # - "traefik.http.routers.traefik-rtr.tls.domains[1].sans=*.$SECONDDOMAINNAME" # Pulls wildcard cert for second domain


      # Dashboard
      - "traefik.http.routers.traefik.rule=Host(`routes.iankeane.dev`)"
      - "traefik.http.routers.traefik.service=api@internal"
      - "traefik.http.routers.traefik.middlewares=admin"
      - "traefik.http.routers.traefik.tls.certresolver=myresolver"
      - "traefik.http.routers.traefik.entrypoints=websecure"
      # echo $(htpasswd -nb user password) | sed -e s/\\$/\\$\\$/g
      - "traefik.http.middlewares.admin.basicauth.users=admin:$$apr1$$NioH//4t$$r4EdeVLXPz0YZBvXMkKNJ/"

      # External Services
      # - "--providers.file=true"
      - "--providers.file.filename=/etc/traefik/servers.toml"
      - "--providers.file.watch=true"


  nginx:
    image: nginx:1.19-alpine
    ports:
      - "81:80"
    volumes:
      - ./nginx/data/conf.d:/etc/nginx/conf.d
      - ./nginx/html:/var/www
    command: "/bin/sh -c 'while :; do sleep 6h & wait $${!}; nginx -s reload; done & nginx -g \"daemon off;\"'"
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.nginx.rule=Host(`iankeane.dev`)"
      - "traefik.http.routers.nginx.entrypoints=websecure"
      - "traefik.http.routers.nginx.tls.certresolver=myresolver"

  rss-bridge:
    volumes:
      - ./rss-bridge/whitelist.txt:/app/whitelist.txt
    build:
      context: ./rss-bridge
    image: rss-bridge
    ports:
      - 3000:80
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.rss-bridge.rule=Host(`iankeane.dev`) && PathPrefix(`/rss-bridge`)"
      - "traefik.http.routers.rss-bridge.middlewares=strip-rss-bridge"
      - "traefik.http.middlewares.strip-rss-bridge.stripprefix.prefixes=/rss-bridge"
      - "traefik.port=3000"
      - "traefik.http.routers.rss-bridge.entrypoints=websecure"
      - "traefik.http.routers.rss-bridge.tls.certresolver=myresolver"

  wiki:
    volumes:
      - ./tiddlywiki/personalwiki:/home/tiddlywiki/personalwiki
    build:
      context: ./tiddlywiki
    image: tiddlywiki
    ports:
      - 9090:8080
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.wiki.rule=Host(`wiki.iankeane.dev`)"
      - "traefik.http.routers.wiki.entrypoints=websecure"
      - "traefik.http.routers.wiki.tls.certresolver=myresolver"

  blog:
    volumes:
      - ./tiddlyblog/personalwiki:/home/tiddlywiki/personalwiki
    build:
      context: ./tiddlyblog
    image: tiddlywiki
    ports:
      - 9092:8080
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.blog.rule=Host(`blog.iankeane.dev`)"
      - "traefik.http.routers.blog.entrypoints=websecure"
      - "traefik.http.routers.blog.tls.certresolver=myresolver"

  searx:
    image: searx/searx:latest
    volumes:
      - ./searx/searx:/etc/searx
    image: searx/searx
    ports:
      - 9999:8080
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.searx.rule=Host(`searx.iankeane.dev`)"
      - "traefik.http.routers.searx.entrypoints=websecure"
      - "traefik.http.routers.searx.tls.certresolver=myresolver"

  nullboard:
    build: ./nullboard
    ports:
      - "3001:80"
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.nullboard.rule=Host(`board.iankeane.dev`)"
      - "traefik.http.routers.nullboard.entrypoints=websecure"
      - "traefik.http.routers.nullboard.tls.certresolver=myresolver"

  bin:
    build: ./bin
    ports:
      - 8000:8000
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.bin.rule=Host(`bin.iankeane.dev`)"
      - "traefik.http.routers.bin.entrypoints=websecure"
      - "traefik.http.routers.bin.tls.certresolver=myresolver"

  prometheus:
    image: prom/prometheus:v2.21.0
    ports:
      - 9191:9090
    volumes:
      - "./prometheus:/etc/prometheus"
      # - prometheus_data:/prometheus
      - "./prometheus/prometheus_data:/data:rw"
    command:
      - '--config.file=/etc/prometheus/prometheus.yml'
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.prometheus.rule=Host(`scrape.iankeane.dev`)"
      - "traefik.http.routers.prometheus.entrypoints=websecure"
      - "traefik.http.routers.prometheus.tls.certresolver=myresolver"
      - "traefik.http.routers.prometheus.middlewares=prom-auth"
      - "traefik.http.middlewares.prom-auth.basicauth.users=admin:$$apr1$$/OtNKjFD$$TcptEYQA7BIK8CuY7f4k8."

  grafana:
    image: grafana/grafana:5.1.0
    ports:
      - 3002:3000
    # If grafana doesn't run as root it can't write to itself
    user: "0:0"
    volumes:
      - ./grafana/grafana_data:/var/lib/grafana:rw
      - ./grafana/provisioning/:/etc/grafana/provisioning:rw
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.grafana.rule=Host(`graf.iankeane.dev`)"
      - "traefik.http.routers.grafana.entrypoints=websecure"
      - "traefik.http.routers.grafana.tls.certresolver=myresolver"

