# Setup

* Go into each directory in this repo and run `make build` to build the images that will be used by docker-compose
    * Currently, tiddlywiki, rss-bridge, and searx need to be built in order to run
* Once this is done, go to the root directory of the project and run `docker-compose up`.

Use `docker-compose build` and `docker-compose run` to run these images

