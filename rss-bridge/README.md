# Setup

Use "Make build" to build the docker image and optionally "Make run" to run the container.

If you run it in a container, you may need to remove that container from your docker daemon or it will interfere with docker-compose

# Whitelisting

Only whitelisted rss-bridges will show up. By default it will use the whitelist file stored in this repo, but to find more to add look at the [rss-bridge bridges page] for ones that interest you. They can be added just by putting a line in the whitelist file with the name of the bridge minus the "bridge.php" at the end.

# Styling

"style.css" allows you to control the CSS of your rss-bridge page. 
> TODO: I need to clean up the one stored here becaues I just jammed the CSS I wanted in after the default CSS with only a couple changes to the default, resulting in a confusing mess.

# Port
    3000
