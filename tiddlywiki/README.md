# Setup

Use "Make build" to build the docker image and optionally "Make run" to run the container.

If you run it in a container, you may need to remove that container from your docker daemon or it will interfere with docker-compose

# Preferences

Preferences are stored in the personalwiki folder in this repo, but should be interacted with through the web interface.

# Port
    9090

> TODO: password-ize this with env variables

